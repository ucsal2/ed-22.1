package lista;


public class Construtor {

	No no = new No();
	No inicio = null;
	int tamanho  = 0;



	//inserir no inicio
	public void inserirInicio(String info) {
		No no = new No();
		no.info = info;
		no.proximo = inicio;
		inicio = no;
		tamanho++;
	}

	//inserir no inicio
	public void inserirInicio2(String info) {
		No novo = new No();
		if(inicio== null){
			inicio = novo;	
		} else {
			No aux =inicio;
			while(aux.proximo!= null) {
				aux = aux.proximo;
			}
			tamanho++;
		}
	}

	//inserir nome 
	public void inserirfim (String info) {
		no.info = info;
		inicio = no;
		if(inicio == null ) {
			inicio = no;
			no.proximo = null;
		} else {
			No local = inicio;
			while(local.proximo != null) {
				local =local.proximo;
			}
			local.proximo = no;
			no.proximo = null;
			tamanho++;
		}

	}


	//tamanho com os elementos
	public String toString() {
		String str = "(" + tamanho + ")" ;
		No local = inicio;
		while (local != null) {
			str += local.info + " " ;
			local = local.proximo;
		}
		return str;
	}

	public String removerPosicao(int posicao) {
		  if(posicao < 0 || posicao >= tamanho || inicio == null) {
		     return null;
		  } else if (posicao == 0) {
		        return retirarInicio();
		  } else if (posicao == tamanho - 1)  {
		        return retirarFim();
		  }
		  No local = inicio;
		  for (int i = 0; i < posicao - 1 ; i++) {
		        local = local.proximo;
		  }
		  String info = local.proximo.info;
		  local.proximo = local.proximo.proximo;
		  tamanho--;
		  return info;
		}



	private String retirarFim() {
		if(inicio == null ) {
			return null;
		} 
		No local = inicio;
		//encontrar o ultimo elemento
		while(local.proximo !=null) {
			No aux = local;
			local=local.proximo;
			if(local.proximo == null ) {
				aux.proximo = null;
				tamanho--;
				return local.info;
			}

		}

		inicio = null;
		tamanho--;
		return local.info;
	}

	private String retirarInicio() {
		// se inicio for null ele retorna null
		if(inicio == null) {
			return null;
		}
		// salva informação para ultilizar dps
		String info = inicio.info;
		//aponto para o proximo no dps do inicio
		inicio=inicio.proximo;
		tamanho--;
		return (info);
	}

	public boolean procura(String info) {
		boolean achou = false;
		No local = inicio;
		for (int i = 0; i < tamanho; i++) {
			if(local.info == info) {
				achou=true;
			}
			local = local.proximo;
		}
		return achou;

	}
}
