package livros;

public class LivrosSequecial implements livro{

	private String [] vetorTitulo;
	private String [] vetorEditora;
	private int [] vetorAno;
	private int a;
	private int b;
	private int c;
	
	
	public LivrosSequecial() {
		vetorAno = new int[10];
		vetorTitulo = new String[10];
		vetorEditora = new String [10];
		a=0;
		b=0;
		c=0;
	}

	@Override
	public void insereTitulo(String titulo) {
		// TODO Auto-generated method stub
		vetorTitulo[a++] = titulo;
		
	}

	@Override
	public void insereEditora(String editora) {
		// TODO Auto-generated method stub
		vetorEditora[b++] = editora;
		
	}

	@Override
	public void insereAnoPubli(int ano) {
		// TODO Auto-generated method stub
		vetorAno [c++] = ano;
		
	}

	@Override
	public String gettitulo(int titulo) {
		// TODO Auto-generated method stub
		return vetorTitulo[titulo];
	}

	@Override
	public String geteditora(int editora) {
		// TODO Auto-generated method stub
		return vetorEditora[editora];
	}

	@Override
	public int getanoPubli(int ano) {
		// TODO Auto-generated method stub
		return vetorAno[ano];
	}

	@Override
	public int tamanhoTitulo() {
		// TODO Auto-generated method stub
		return a;
	}

	@Override
	public int tamanhoEditora() {
		// TODO Auto-generated method stub
		return b;
	}

	@Override
	public int tamanhoAno() {
		// TODO Auto-generated method stub
		return c;
	}

	
	
}
