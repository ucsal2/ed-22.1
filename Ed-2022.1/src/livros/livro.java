package livros;

public interface livro {

	void insereTitulo(String titulo);
	void insereEditora(String editora);
	void insereAnoPubli(int ano);
	
	String gettitulo (int titulo);
	String geteditora (int editora);
	int getanoPubli (int ano);
	
	int tamanhoTitulo();
	int tamanhoEditora();
	int tamanhoAno();
}
