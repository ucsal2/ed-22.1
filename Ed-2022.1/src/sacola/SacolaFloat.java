package sacola;

interface SacolaFloat{
	
	void insere(float item);
	
	float get(int i);
	
	int tamanho();
	
}