package sacola;

public class SacolaFloatSequencial implements SacolaFloat {

	private float [] vetor;
	private int n;


	public  SacolaFloatSequencial (){
		vetor = new float[10];
		n = 0;
	}

	
	public void insere(float item) {
		vetor[n++] = item;

	}

	
	public float get(int i) {

		return vetor[i];
	}

	
	public int tamanho() {

		return n;
	}



}

