package profprof;

public class ExercicioListaSimples {
	No inicio = null;
	int tamanho = 0;

	public void inserirInicio(String info) {
		No no = new No();
		no.info = info;
		no.proximo = inicio;
		inicio = no;
		tamanho++;
	}

	public String toString() {
		String str = "(Tamanho=" + tamanho + ")  Lista: " ;
		No local = inicio;
		while (local != null) {
			str += local.info + " " ;
			local = local.proximo;
		}
		return str;
	}

	public void inserirFim(String info) {
		No no = new No();
		no.info = info;
		if(inicio == null) {
			// nao existe ainda um n�
			no.proximo = null;
			inicio = no;
		} else {
			// ja existe um n� na lista
			No local = inicio;
			// chegar at� o �ltimo no
			while (local.proximo != null) {
				local = local.proximo;
			}
			local.proximo = no;
			no.proximo = null;
		}
		tamanho++;
	}

	public String tamanhoLista() {
		String str = "Tamanho da Lista = " + tamanho;
		return str;
	}

	public String retirarInicio() {
		if(inicio == null) {
			return null;
		}
		// Salva informa��o para utilizar depois
		String info = inicio.info;
		// aponta o inicio para o pr�ximo dele
		inicio = inicio.proximo;
		tamanho--;
		return(info);
	}


	public String retirarFim() {
		if(inicio == null) {
			return null;
		}
		No local = inicio;
		while (local.proximo != null ) {
			No aux = local;
			local = local.proximo;
			if (local.proximo == null) {
				aux.proximo = null;
				tamanho--;
				return local.info;
			}
		}
		inicio = null;
		tamanho--;
		return local.info;

	}



	public String removerPosicao(int posicao) {
		if(posicao < 0 || posicao >= tamanho || inicio == null) {
			return null;
		} else if (posicao == 0) {
			return retirarInicio();
		} else if (posicao == tamanho - 1)  {
			return retirarFim();
		}
		No local = inicio;
		for (int i = 0; i < posicao - 1 ; i++) {
			local = local.proximo;
		}
		String info = local.proximo.info;
		local.proximo = local.proximo.proximo;
		tamanho--;
		return info;
	}

	public boolean nomeNaLista(String info) {
		No local = inicio;
		boolean achou = false;
		for (int i = 0; i < tamanho ; i++) {
			if(local.info == info) {
				achou = true;
			}
			local = local.proximo;
		}
		return achou;
	}





}