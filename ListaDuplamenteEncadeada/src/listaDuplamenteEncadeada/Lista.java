package listaDuplamenteEncadeada;


public class Lista implements  ListaDuplamenteEncade{

	No inicio;
	No  fim;
	int tamanho;

	public void InserirInicio(String info) {
		No no = new No();
		no.info = info;
		no.ant = null;
		no.prox = inicio;
		if(inicio == null) {
			inicio.ant = no;
		}
		inicio = no;
		if(tamanho == 0) {
			fim = inicio;
		}
		tamanho++;

	}
	public String toString() {
		String str = "(" + tamanho + ") " ;
		No local = inicio;
		while (local != null) {
			str += local.info + "|" ;
			local = local.prox;
		}
		return str;
	}
	@Override
	public int size() {
		// retorna taqmanho da lista
		return tamanho;
	}

}
