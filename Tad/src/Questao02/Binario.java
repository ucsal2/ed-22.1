package Questao02;

public class Binario {

	public static void main(String[] args) {
		decimalParaBinario(9);
	}

	
	public static void decimalParaBinario(int n) {
		if(n>0) {
			decimalParaBinario(n/2);
			System.out.print(n%2);
		}
	}
}
