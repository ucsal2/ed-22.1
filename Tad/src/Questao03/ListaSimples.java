package Questao03;


public class ListaSimples {
	No inicio = null;
	int tamanho = 0;

	public void inserir(String info) {
		No no = new No();
		no.info = info;
		if(inicio == null) {
			// nao existe ainda um n�
			no.proximo = null;
			inicio = no;
		} else {
			// ja existe um n� na lista
			No local = inicio;
			// chegar at� o �ltimo no
			while (local.proximo != null) {
				local = local.proximo;
			}
			local.proximo = no;
			no.proximo = null;
		}
		tamanho++;
	}
	public String toString() {
		String str = "(Tamanho=" + tamanho + ")  Lista: " ;
		No local = inicio;
		while (local != null) {
			str += local.info + " | " ;
			local = local.proximo;
		}
		return str;
	}

}
